
;;; yuck-mode.el --- major mode for yuck, the eww language -*- lexical-binding: t; -*-
;;; Commentary:
;; 
;;; Code:
;;;; Syntax Table
;; Not sure if it's better to just inherit lisp-data-mode, but this
;; works fine for now, so I won't touch it.
(defvar yuck-mode-syntax-table
  (let ((table (make-syntax-table))
	(i 0))
    (while (< i ?0)
      (modify-syntax-entry i "_   " table)
      (setq i (1+ i)))
    (setq i (1+ ?9))
    (while (< i ?A)
      (modify-syntax-entry i "_   " table)
      (setq i (1+ i)))
    (setq i (1+ ?Z))
    (while (< i ?a)
      (modify-syntax-entry i "_   " table)
      (setq i (1+ i)))
    (setq i (1+ ?z))
    (while (< i 128)
      (modify-syntax-entry i "_   " table)
      (setq i (1+ i)))
    (modify-syntax-entry ?\s "    " table)
    (modify-syntax-entry ?\" "\"   " table)
    (modify-syntax-entry ?\( "()   " table)
    (modify-syntax-entry ?\) ")(   " table)
    (modify-syntax-entry ?\{ "(}   " table)
    (modify-syntax-entry ?\} "){   " table)
    (modify-syntax-entry ?\; "<   " table)
    (modify-syntax-entry ?\n ">   " table) table)
  "Syntax table used in `yuck-mode'.")
;;;; Font-Lock Keywords
(defconst yuck-mode-font-lock-keywords
  '(("[[:space:]]:[-_A-Za-z0-9]+" . font-lock-builtin-face)
    ("defwidget\\|defwindow\\|defpoll\\|defvar\\|deflisten\\|geometry\\|struts\\|include\\|box\\|button" . font-lock-keyword-face)))
;;;; Completion
(defvar yuck-keywords
  '(":anchor" ":class" ":distance" ":exclusive" ":focusable" ":geometry" ":halign" ":height" ":initial" ":interval" ":monitor" ":name" ":nth" ":onclick" ":reserve" ":run-while" ":side" ":stacking" ":stacking" ":text" ":width" ":windowtype" ":wm-ignore" ":x" ":y" "bottom" "box" "button" "center" "children" "deflisten" "defpoll" "defvar" "defwidget" "defwindow" "desktop" "dialog" "dock" "false" "label" "left" "normal" "overlay" "right" "toolbar" "top" "true"))
(defun yuck-completion-at-point ()
  "Function used for `completion-at-point-functions' in `yuck-mode'"
  (interactive)
  (let* ((bds (bounds-of-thing-at-point 'symbol))
         (start (car bds))
         (end (cdr bds)))
    (list start end yuck-keywords . nil )))
;;;; Abbrev Table
(defvar yuck-mode-abbrev-table nil)
;;;; Hooks
(defvar yuck-mode-hook nil "Hook to run when entering yuck-mode.")
;;;; The Mode
(add-to-list 'auto-mode-alist '("\\.yuck\\'" . yuck-mode))
;;;###autoload
(define-derived-mode yuck-mode prog-mode "Yuck"
  "Major mode for editing Yuck code.
Editing commands are similar to those of `lisp-mode'."
  (setq-local font-lock-defaults '((yuck-mode-font-lock-keywords)))
  (set-syntax-table lisp-mode-syntax-table)
  (setq-local comment-start "; ")
  (setq-local comment-end "")
  (add-hook 'completion-at-point-functions 'yuck-completion-at-point)
  (run-hooks 'yuck-mode-hook))
(provide 'yuck-mode)
;;; yuck-mode.el ends here
